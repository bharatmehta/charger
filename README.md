#charger

Develop a REST Interface for storage and retrieval of charging station data.
The following information needs to be stored per charging station:
• Unique ID (is user-defned)
• Geo-Coordinates
• Zipcode / Postal Code
The interface should provide the following functionality:
• Persist charging stations
• Retrieve/Query charging stations by
◦ ID
◦ Zipcode / Postal Code
◦ a perimeter around a given geolocation

Write some test cases that cover the functionality adequately.
Pay special attention to:
• REST conventions
• Structure of the source code
• OOP and related principles (SOLID, DRY)
• Documentation and comprehensibility
• Quality Assurance


## Technology Stack
- Java 8
- Spring Boot
- JUnit
- Mockito
- Swagger UI
- Mongodb
- Lombok
- Maven
- Docker
- Embedded Mongodb 

## Build instructions

Make sure you are not running any Mongodb instances
````
mvn clean install
docker-compose -f mongodb-docker.yml up
````

## Running
Make sure you don't have any Mongodb instance running.

- Start the docker container
````
docker-compose -f mongodb-docker.yml up

````
- Start the spring boot application
````
java -jar target/charger-0.0.1-SNAPSHOT.jar

````

- Goto [Swagger-UI](http://localhost:8080/swagger-ui.html)

## TODO
- Docker image for the application
- Fix the ChargingStationDto.Location
- Search api should be seperate api powered by ElasticStack. If use case expands, this will help
- Use Protocol Buffer for exchange of data

