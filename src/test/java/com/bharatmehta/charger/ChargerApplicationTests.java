package com.bharatmehta.charger;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * For checking Application Context
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ChargerApplicationTests {


    @Test
    public void contextLoads() {
    }


}
