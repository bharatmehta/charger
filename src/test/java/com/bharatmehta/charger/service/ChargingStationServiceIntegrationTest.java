package com.bharatmehta.charger.service;

import com.bharatmehta.charger.model.ChargingStation;
import com.bharatmehta.charger.repository.ChargingStationRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test for {@link ChargingStationServiceImpl}
 */
@SpringBootTest
public class ChargingStationServiceIntegrationTest {


    @Autowired
    private ChargingStationService service;

    @Autowired
    private ChargingStationRepository repository;


    @BeforeEach
    public void setup() {
        repository.deleteAll();
    }


    @Test
    public void testSave() {
        final String UNIQUE_ID = UUID.randomUUID().toString();
        ChargingStation station = new ChargingStation();
        station.setUniqueIdentifier(UNIQUE_ID);
        station.setZipCode("10123");
        station.setLocation(new GeoJsonPoint(10, 10));
        Assertions.assertThat(service.create(station)).isNotNull();
    }


    @Test
    public void testFindByUniqueIdentifer() {
        final String UNIQUE_ID = UUID.randomUUID().toString();
        ChargingStation station = new ChargingStation();
        station.setUniqueIdentifier(UNIQUE_ID);
        station.setZipCode("10123");
        station.setLocation(new GeoJsonPoint(10, 10));

        Assertions.assertThat(service.create(station)).isNotNull();

        Optional<ChargingStation> byId = service.findByUniqueIdentifier(station.getUniqueIdentifier());
        assertThat(byId).isPresent();
    }


    @Test
    public void testRemoveByUniqueIdentifier() {
        final String UNIQUE_ID = UUID.randomUUID().toString();
        ChargingStation station = new ChargingStation();
        station.setUniqueIdentifier(UNIQUE_ID);
        station.setZipCode("10123");
        station.setLocation(new GeoJsonPoint(10, 10));

        Assertions.assertThat(service.create(station)).isNotNull();

        service.remove(station.getUniqueIdentifier());
        assertThat(repository.findAll().isEmpty()).isTrue();
    }

    @Test
    public void testUpdate() {

        final String UNIQUE_ID = UUID.randomUUID().toString();
        ChargingStation station = new ChargingStation();
        station.setUniqueIdentifier(UNIQUE_ID);
        station.setZipCode("10123");
        station.setLocation(new GeoJsonPoint(10, 10));
        repository.save(station);

        station.setZipCode("10124");
        service.update(station);
        Optional<ChargingStation> updated = repository.findByUniqueIdentifier(UNIQUE_ID);
        assertThat(updated.isPresent()).isTrue();
        assertThat(updated.get().getZipCode()).isEqualTo("10124");
    }

    @Test
    public void testFindByZipCode() {
        final String UNIQUE_ID = UUID.randomUUID().toString();
        ChargingStation station = new ChargingStation();
        station.setUniqueIdentifier(UNIQUE_ID);
        station.setZipCode("10123");
        station.setLocation(new GeoJsonPoint(10, 10));
        repository.save(station);

        List<ChargingStation> result = service.findByZipCode(station.getZipCode());
        assertThat(result.size()).isEqualTo(1);
        assertThat(result.get(0).getZipCode()).isEqualTo(station.getZipCode());
    }


    @Test
    public void testFindByLocationNear() {
        ChargingStation brandenBurgGate = new ChargingStation();
        brandenBurgGate.setUniqueIdentifier("1ABC");
        brandenBurgGate.setZipCode("10117");
        brandenBurgGate.setLocation(new GeoJsonPoint(52.518076, 13.370258));
        repository.save(brandenBurgGate);

        Point point = new Point(52.4812497, 13.3535755);
        List<ChargingStation> result = service.findNearLocation(point, 20);
        assertThat(result.size()).isEqualTo(1);
        assertThat(result.get(0).getZipCode()).isEqualTo(brandenBurgGate.getZipCode());
    }

    @Test
    public void testFindByZipCodeAndLocationNear() {
        ChargingStation brandenBurgGate = new ChargingStation();
        brandenBurgGate.setUniqueIdentifier("1ABC");
        brandenBurgGate.setZipCode("10117");
        brandenBurgGate.setLocation(new GeoJsonPoint(52.518076, 13.370258));
        repository.save(brandenBurgGate);

        Point point = new Point(52.4812497, 13.3535755);
        List<ChargingStation> result = service.findByZipCodeAndLocation("10117", point,
                20.00);
        assertThat(result.size()).isEqualTo(1);
        assertThat(result.get(0).getLocation()).isEqualTo(brandenBurgGate.getLocation());
    }


}
