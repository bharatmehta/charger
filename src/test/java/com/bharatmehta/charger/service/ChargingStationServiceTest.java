package com.bharatmehta.charger.service;

import com.bharatmehta.charger.exception.ChargingStationNotFoundException;
import com.bharatmehta.charger.model.ChargingStation;
import com.bharatmehta.charger.repository.ChargingStationRepository;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link ChargingStationServiceImpl}
 */
@ExtendWith(MockitoExtension.class)
public class ChargingStationServiceTest {


    @Mock
    private ChargingStationRepository chargingStationRepository;

    private ChargingStationService chargingStationService;

    @BeforeEach
    public void setup() {
        chargingStationService = new ChargingStationServiceImpl(chargingStationRepository);
    }

    @Test
    public void testNull() {
        assertThrows(IllegalArgumentException.class, () -> new ChargingStationServiceImpl(null));
    }

    @Test
    public void testSave_NullId() {
        ChargingStation station = new ChargingStation();
        station.setLocation(new GeoJsonPoint(0.00, 0.00));
        station.setUniqueIdentifier(null);
        station.setZipCode("ABC12");
        assertThrows(IllegalArgumentException.class, () -> chargingStationService.create(station));
        verify(chargingStationRepository, never()).findByUniqueIdentifier(station.getUniqueIdentifier());
        verify(chargingStationRepository, never()).save(station);
    }

    @Test
    public void testSave_Null() {
        assertThrows(IllegalArgumentException.class, () -> chargingStationService.create(null));
    }

    @Test
    public void testSave_InvalidCoordinate() {
        ChargingStation station = new ChargingStation();
        station.setUniqueIdentifier("ABC12");
        station.setZipCode("ABC12");
        station.setLocation(null);
        assertThrows(IllegalArgumentException.class, () -> chargingStationService.create(station));
        verify(chargingStationRepository, never()).findByUniqueIdentifier(station.getUniqueIdentifier());
        verify(chargingStationRepository, never()).save(station);
    }


    @Test
    public void testSave_IllegalLatitude() {
        ChargingStation station = new ChargingStation();
        station.setLocation(new GeoJsonPoint(278972987297.00, 0.00));
        station.setUniqueIdentifier("ABC12");
        station.setZipCode("ABC12");
        assertThrows(IllegalArgumentException.class, () -> chargingStationService.create(station));
        verify(chargingStationRepository, never()).findByUniqueIdentifier(station.getUniqueIdentifier());
        verify(chargingStationRepository, never()).save(station);
    }

    @Test
    public void testSave_IllegalLongitude() {
        ChargingStation station = new ChargingStation();
        station.setLocation(new GeoJsonPoint(0.00, 292980280.00));
        station.setUniqueIdentifier("ABC12");
        station.setZipCode("ABC12");
        assertThrows(IllegalArgumentException.class, () -> chargingStationService.create(station));
        verify(chargingStationRepository, never()).findByUniqueIdentifier(station.getUniqueIdentifier());
        verify(chargingStationRepository, never()).save(station);
    }


    @Test
    public void testSave_Valid() {
        ChargingStation station = new ChargingStation();
        station.setLocation(new GeoJsonPoint(0.00, 0.00));
        station.setUniqueIdentifier("ABC");
        station.setZipCode("ABC12");
        when(chargingStationRepository.findByUniqueIdentifier(station.getUniqueIdentifier())).thenReturn(Optional.empty());
        when(chargingStationRepository.save(station)).thenReturn(station);
        assertThat(chargingStationService.create(station)).isNotNull();
        verify(chargingStationRepository, times(1)).findByUniqueIdentifier(station.getUniqueIdentifier());
        verify(chargingStationRepository, times(1)).save(station);
    }

    @Test
    public void testSave_InvalidId() {
        ChargingStation station = new ChargingStation();
        station.setLocation(new GeoJsonPoint(0.00, 0.00));
        station.setUniqueIdentifier("  ");
        station.setZipCode("ABC12");
        assertThrows(IllegalArgumentException.class, () -> chargingStationService.create(station));
        verify(chargingStationRepository, never()).findByUniqueIdentifier(station.getUniqueIdentifier());
        verify(chargingStationRepository, never()).save(station);
    }


    @Test
    public void testFindByIdentifier_Null() {
        assertThrows(IllegalArgumentException.class, () -> chargingStationService.findByUniqueIdentifier(null));
    }

    @Test
    public void testFindByIdentifier_Blank() {
        assertThrows(IllegalArgumentException.class, () -> chargingStationService.findByUniqueIdentifier("  "));
    }

    @Test
    public void testFindById_Success() {
        final String ID = "ABC123";
        when(chargingStationRepository.findByUniqueIdentifier(ID)).thenReturn(Optional.empty());
        Optional<ChargingStation> stationById = chargingStationService.findByUniqueIdentifier(ID);
        assertThat(stationById.isPresent()).isFalse();

    }

    @Test
    public void testRemove_ValidId() {
        final String ID = "ABC123";
        ChargingStation station = new ChargingStation();
        station._id = new ObjectId();
        station.setUniqueIdentifier(ID);
        station.setZipCode("12398");
        station.setLocation(new GeoJsonPoint(90.00, 90.00));
        when(chargingStationRepository.findByUniqueIdentifier(ID)).thenReturn(Optional.of(station));
        chargingStationService.remove(ID);
        verify(chargingStationRepository, times(1)).deleteById(station._id.toHexString());
    }

    @Test
    public void testRemove_NonExistent() {
        final String ID = "ABC123";
        when(chargingStationRepository.findByUniqueIdentifier(ID)).thenReturn(Optional.empty());
        assertThrows(ChargingStationNotFoundException.class,
                () -> chargingStationService.remove(ID));
        verify(chargingStationRepository, never()).deleteById(any(String.class));
    }


    @Test
    public void testUpdate_NotFound() {
        ChargingStation station = new ChargingStation();
        station._id = new ObjectId();
        station.setUniqueIdentifier("ID");
        station.setZipCode("12398");
        station.setLocation(new GeoJsonPoint(90.00, 90.00));
        when(chargingStationRepository.findByUniqueIdentifier(station.getUniqueIdentifier()))
                .thenReturn(Optional.empty());
        assertThrows(ChargingStationNotFoundException.class, () ->chargingStationService.update(station));
        verify(chargingStationRepository, never()).save(station);

    }


    @Test
    public void testUpdate_Found() {
        ChargingStation station = new ChargingStation();
        station._id = new ObjectId();
        station.setUniqueIdentifier("ID");
        station.setZipCode("12398");
        station.setLocation(new GeoJsonPoint(90.00, 90.00));
        when(chargingStationRepository.findByUniqueIdentifier(station.getUniqueIdentifier()))
                .thenReturn(Optional.of(station));
        chargingStationService.update(station);
        verify(chargingStationRepository, times(1)).save(station);

    }

    @Test
    public void testFindByInvalidZipCode() {
        assertThrows(IllegalArgumentException.class,
                () -> chargingStationService.findByZipCode(null));
        assertThrows(IllegalArgumentException.class,
                () -> chargingStationService.findByZipCode(" "));
    }

    @Test
    public void testFindByZipCodeAndPoint_InvalidCalls() {
        assertThrows(IllegalArgumentException.class,
                () -> chargingStationService.findByZipCodeAndLocation(null,
                        new Point(10, 10), 6));
        assertThrows(IllegalArgumentException.class,
                () -> chargingStationService.findByZipCodeAndLocation(" ",
                        new Point(10, 10), 6));

        assertThrows(IllegalArgumentException.class,
                () -> chargingStationService.findByZipCodeAndLocation(" ",
                        new Point(10, 10), 6));
        assertThrows(IllegalArgumentException.class,
                () -> chargingStationService.findByZipCodeAndLocation("18282",
                        new Point(10, 10), -6));
    }
}
