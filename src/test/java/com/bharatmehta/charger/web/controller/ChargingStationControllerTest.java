package com.bharatmehta.charger.web.controller;

import com.bharatmehta.charger.exception.ChargingStationNotFoundException;
import com.bharatmehta.charger.model.ChargingStation;
import com.bharatmehta.charger.service.ChargingStationService;
import com.bharatmehta.charger.web.transferobject.ChargingStationDto;
import com.bharatmehta.charger.web.transferobject.ChargingStationMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.bharatmehta.charger.exception.DuplicateChargingStationException;
import com.bharatmehta.charger.repository.ChargingStationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Tests for {@link ChargingStationController}
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(ChargingStationController.class)
public class ChargingStationControllerTest {

    private final String PATH = "/chargingstation";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private ChargingStationService chargingStationService;

    @MockBean
    private ChargingStationRepository chargingStationRepository;

    private ChargingStationDto request;

    @BeforeEach
    public void setup() {
        request = chargingStationDto();
    }

    @Test
    void post_ChargingStation() throws Exception {
        given(chargingStationService.create(ChargingStationMapper.domain(request)))
                .willReturn(ChargingStationMapper.domain(request));
        postAndExpect(request).andExpect(status().isCreated());
    }

    @Test
    void post_NoBody() throws Exception {
        postAndExpect("{}").andExpect(status().isBadRequest());
        ;
    }

    @Test
    void post_UniqueIdIsNull() throws Exception {
        request.setUniqueIdentifier(null);
        postAndExpect(request).andExpect(status().isBadRequest());

    }

    @Test
    void post_UniqueIdIsBlank() throws Exception {
        request.setUniqueIdentifier("");
        postAndExpect(request).andExpect(status().isBadRequest());
    }


    @Test
    void post_LocationIsNull() throws Exception {
        request.setLocation(null);
        postAndExpect(request).andExpect(status().isBadRequest());
    }

    @Test
    void post_ZipCodeIsNull() throws Exception {
        request.setZipCode(null);
        postAndExpect(request).andExpect(status().isBadRequest());
    }


    @Test
    void post_DuplicateChargingStation() throws Exception {

        given(chargingStationService.create(ArgumentMatchers.any(ChargingStation.class)))
                .willThrow(DuplicateChargingStationException.class);

        String requestBody = mapper.writeValueAsString(request);
        mockMvc.perform(post(PATH)
                .contentType(APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isConflict());

    }


    @Test
    void delete_StationNotFound() throws Exception {
        Mockito.doThrow(new ChargingStationNotFoundException()).
                when(chargingStationService).remove(request.getUniqueIdentifier());
        mockMvc.perform(delete(PATH + "/" + request.getUniqueIdentifier())
                .contentType(APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void put_ChargingStation() throws Exception {
        given(chargingStationService.findByUniqueIdentifier(request.getUniqueIdentifier()))
                .willReturn(Optional.empty());
        given(chargingStationService.create(ChargingStationMapper.domain(request)))
                .willReturn(ChargingStationMapper.domain(request));
        putAndExpect(request).andExpect(status().isCreated());

    }

    @Test
    void put_ChargingStationUpdated() throws Exception {
        given(chargingStationService.findByUniqueIdentifier(request.getUniqueIdentifier()))
                .willReturn(Optional.of(ChargingStationMapper.domain(request)));
        given(chargingStationService.update(ChargingStationMapper.domain(request)))
                .willReturn(ChargingStationMapper.domain(request));
        putAndExpect(request).andExpect(status().isOk());

    }

    @Test
    void put_NoBody() throws Exception {
        putAndExpect("{}").andExpect(status().isBadRequest());
    }

    @Test
    void put_UniqueIdIsNull() throws Exception {
        request.setUniqueIdentifier(null);
        putAndExpect(request).andExpect(status().isBadRequest());
    }

    @Test
    void put_UniqueIdIsBlank() throws Exception {
        request.setUniqueIdentifier("");
        putAndExpect(request).andExpect(status().isBadRequest());
    }


    @Test
    void put_LocationIsNull() throws Exception {
        request.setLocation(null);
        putAndExpect(request).andExpect(status().isBadRequest());
    }

    @Test
    void put_ZipCodeIsNull() throws Exception {
        request.setZipCode(null);
        putAndExpect(request).andExpect(status().isBadRequest());
    }


    private ChargingStationDto chargingStationDto() {
        ChargingStationDto request = new ChargingStationDto();
        request.setUniqueIdentifier("ABC");
        request.setLocation(new ChargingStationDto.Location(90.00, 90.00));
        request.setZipCode("ABC38783793");
        return request;
    }

    private String requestBody(final ChargingStationDto dto) throws Exception {
        String json = mapper.writeValueAsString(dto);
        System.out.println(json);
        return json;
    }


    private ResultActions postAndExpect(ChargingStationDto request) throws Exception {
        String requestBody = requestBody(request);

        return mockMvc.perform(post(PATH)
                .contentType(APPLICATION_JSON)
                .content(requestBody));


    }

    private ResultActions putAndExpect(ChargingStationDto request) throws Exception {
        String requestBody = requestBody(request);

        return mockMvc.perform(put(PATH)
                .contentType(APPLICATION_JSON)
                .content(requestBody));


    }


    private ResultActions postAndExpect(String requestBody) throws Exception {
        return mockMvc.perform(post(PATH)
                .contentType(APPLICATION_JSON)
                .content(requestBody));

    }

    private ResultActions putAndExpect(String requestBody) throws Exception {
        return mockMvc.perform(post(PATH)
                .contentType(APPLICATION_JSON)
                .content(requestBody));

    }
}
