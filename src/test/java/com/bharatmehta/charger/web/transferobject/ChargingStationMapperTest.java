package com.bharatmehta.charger.web.transferobject;

import com.bharatmehta.charger.model.ChargingStation;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests for ChargingStationMapper
 */
public class ChargingStationMapperTest {


    @Test
    public void test_dto() {
        ChargingStation domain = new ChargingStation();
        domain.setUniqueIdentifier("ABC");
        domain.setLocation(new GeoJsonPoint(90.00, 90.98));
        domain.set_id(new ObjectId());

        ChargingStationDto dto = ChargingStationMapper.dto(domain);
        assertThat(dto.getUniqueIdentifier()).isEqualTo(domain.getUniqueIdentifier());
        assertThat(dto.getZipCode()).isEqualTo(domain.getZipCode());
        assertThat(dto.getLocation().getLon()).isEqualTo(domain.getLocation().getX());
        assertThat(dto.getLocation().getLat()).isEqualTo(domain.getLocation().getY());

    }

    @Test
    public void test_domain() {
        ChargingStationDto dto = new ChargingStationDto();
        dto.setUniqueIdentifier("ABC");
        dto.setLocation(new ChargingStationDto.Location(90.00, 90.98));

        ChargingStation domain = ChargingStationMapper.domain(dto);
        assertThat(domain.getUniqueIdentifier()).isEqualTo(dto.getUniqueIdentifier());
        assertThat(domain.getZipCode()).isEqualTo(dto.getZipCode());
        assertThat(domain.getLocation().getX()).isEqualTo(dto.getLocation().getLon());
        assertThat(domain.getLocation().getY()).isEqualTo(dto.getLocation().getLat());
    }
}
