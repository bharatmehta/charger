package com.bharatmehta.charger.web.transferobject;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * DTO
 */
@Getter
@Setter
@NoArgsConstructor
public class ChargingStationDto {

    @NotBlank(message = "uniqueIdentifier can't be null")
    private String uniqueIdentifier;

    @NotBlank(message = "zipCode can't be null")
    private String zipCode;

    @NotNull(message = "location can't be null")
    private Location location;

    /**
     * Location for a ChargingStation
     */
    @Getter
    @Setter
    @AllArgsConstructor
    public static class Location {

        private double lon;

        private double lat;

    }

}
