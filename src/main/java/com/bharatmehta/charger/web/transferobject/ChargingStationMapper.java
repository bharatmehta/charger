package com.bharatmehta.charger.web.transferobject;

import com.bharatmehta.charger.model.ChargingStation;
import com.bharatmehta.charger.web.transferobject.ChargingStationDto.Location;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

/**
 * {@link ChargingStationDto} <-> {@link ChargingStation} mapper
 */
public final class ChargingStationMapper {


    private ChargingStationMapper() {
    }

    public static ChargingStationDto dto(final ChargingStation domain) {
        ChargingStationDto dto = new ChargingStationDto();
        dto.setLocation(
                new Location(domain.getLocation().getX(),
                        domain.getLocation().getY()));
        dto.setZipCode(domain.getZipCode());
        dto.setUniqueIdentifier(domain.getUniqueIdentifier());
        return dto;
    }

    public static ChargingStation domain(final ChargingStationDto dto) {
        ChargingStation domain = new ChargingStation();
        domain.setZipCode(dto.getZipCode());
        domain.setLocation(
                new GeoJsonPoint(dto.getLocation().getLon(),dto.getLocation().getLat()));
        domain.setUniqueIdentifier(dto.getUniqueIdentifier());
        return domain;
    }
}
