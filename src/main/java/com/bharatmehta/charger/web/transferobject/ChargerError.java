package com.bharatmehta.charger.web.transferobject;

import lombok.Value;

/**
 * Error messages
 */
@Value
public class ChargerError {
    public static final ChargerError DUPLICATE_CHARGING_STATION = new ChargerError("Duplicate Charging Station");
    public static final ChargerError CHARGING_STATION_NOT_FOUND = new ChargerError("Charging Station not found");
    public static final ChargerError INVALID_REQUEST_FORMAT = new ChargerError("Invalid Format");

    String message;

}
