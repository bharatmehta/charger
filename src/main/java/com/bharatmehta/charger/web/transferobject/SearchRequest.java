package com.bharatmehta.charger.web.transferobject;

import lombok.Getter;
import lombok.Setter;

/**
 * Search Request DTO
 */
@Getter
@Setter
public class SearchRequest {

    private String zipCode;

    private Perimeter perimeter;
}
