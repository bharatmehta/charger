package com.bharatmehta.charger.web.transferobject;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * DTO for mentioning the Perimeter of the search
 */
@Getter
@Setter
public class Perimeter {

    @NotNull
    @Range(min = -180, max = 180, message = "Longitude/x should be in range of -180.00 to +180.00")
    private Double lon;

    @NotNull
    @Range(min = -90, max = 90, message = "Latitude/y should be in range of -90.00 to +90.00")
    private Double lat;

    @NotNull
    @Min(value = 0, message = "Radius can not be negative")
    private Double radiusKm;

    /**
     * If the radiusKm is not set, then default radiusKm is 5 Kms
     * @return
     */
    public Double getRadiusKm(){
        if(lon != null && lat != null && radiusKm == null){
            radiusKm = 5.00;
        }
        return radiusKm;
    }


    public boolean isValid(){
        return lon != null && lat != null && getRadiusKm() != null;
    }

}
