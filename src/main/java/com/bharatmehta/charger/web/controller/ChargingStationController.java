package com.bharatmehta.charger.web.controller;

import com.bharatmehta.charger.model.ChargingStation;
import com.bharatmehta.charger.service.ChargingStationService;
import com.bharatmehta.charger.web.transferobject.ChargingStationDto;
import com.bharatmehta.charger.web.transferobject.ChargingStationMapper;
import com.bharatmehta.charger.web.transferobject.Perimeter;
import com.bharatmehta.charger.web.transferobject.SearchRequest;
import com.google.common.base.Preconditions;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.geo.Point;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/chargingstation")
public class ChargingStationController {


    private final ChargingStationService service;


    public ChargingStationController(final ChargingStationService service) {
        Preconditions.checkArgument(service != null, "service can not be null");
        this.service = service;
    }

    @ApiResponses({
            @ApiResponse(code = 200, message = "ChargingStation is created"),
            @ApiResponse(code = 409, message = "ChargingStation already exists"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 500, message = "Unexpected Error")
    })
    @ApiOperation(value = "Creates a new ChargingStation")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Void> create(@RequestBody @Valid final ChargingStationDto station) {
        ChargingStation create = ChargingStationMapper.domain(station);
        service.create(create);
        HttpHeaders headers = new HttpHeaders();
        return ResponseEntity.created(MvcUriComponentsBuilder
                .fromMethodName(ChargingStationController.class, "findById", create.getUniqueIdentifier())
                .buildAndExpand(create.getUniqueIdentifier()).toUri()).build();
    }

    @ApiResponses({
            @ApiResponse(code = 200, message = "ChargingStation is updated"),
            @ApiResponse(code = 201, message = "ChargingStation is created"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 500, message = "Unexpected Error")
    })
    @ApiOperation(value = "Saves or Updates a ChargingStation")
    @PutMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Void> saveOrUpdate(@RequestBody @Valid final ChargingStationDto station) {
        ChargingStation updates = ChargingStationMapper.domain(station);
        if(service.findByUniqueIdentifier(station.getUniqueIdentifier()).isPresent()){
            service.update(updates);
            return ResponseEntity.status(HttpStatus.OK).build();
        }else{
            return create(station);
        }

    }


    @ApiResponses({
            @ApiResponse(code = 200, message = "ChargingStation is deleted"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Unexpected Error")
    })
    @ApiOperation("Deletes a ChargingStation.To KISS,deletion is hard.")
    @DeleteMapping(value = "/{uniqueIdentifier}")
    public ResponseEntity<Void> delete(@PathVariable("uniqueIdentifier") final String uniqueIdentifier) {
        service.remove(uniqueIdentifier);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @ApiResponses({@ApiResponse(code = 200, message = "Gets a ChargingStation"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Unexpected Error")
    })
    @ApiOperation("Gets a ChargingStation.")
    @GetMapping(value = "/{uniqueIdentifier}")
    public ResponseEntity<ChargingStationDto> findById(@PathVariable("uniqueIdentifier") final String uniqueIdentifier) {
        Optional<ChargingStation> byId = service.findByUniqueIdentifier(uniqueIdentifier);

        if (byId.isPresent()) {
            ChargingStationDto dto = ChargingStationMapper.dto(byId.get());
            return ResponseEntity.ok().body(dto);

        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @ApiResponses({
            @ApiResponse(code = 200, message = "ChargingStation(s) found"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 500, message = "Unexpected Error")
    })
    @ApiOperation("Searches for ChargingStation(s)")
    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<ChargingStationDto>> search(
            @ModelAttribute @Valid SearchRequest request) {

        final Perimeter perimeter = request.getPerimeter();
        List<ChargingStationDto> resultBody = new ArrayList<>();
        if (perimeter != null && perimeter.isValid()) {
            Point point = new Point(perimeter.getLon(), perimeter.getLat());

            if (StringUtils.hasText(request.getZipCode())) {
                resultBody = searchByZipCodeAndLocationNear(request, perimeter, point);
            } else {
                resultBody = geoSpatialSearch(perimeter, point);
            }

        } else if (StringUtils.hasText(request.getZipCode())) {
            resultBody = searchByZipCode(request);
        }
        if (resultBody.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok(resultBody);
        }


    }

    private List<ChargingStationDto> searchByZipCode(SearchRequest request) {
        return service.findByZipCode(request.getZipCode())
                .stream().map(ChargingStationMapper::dto)
                .collect(Collectors.toList());
    }

    private List<ChargingStationDto> searchByZipCodeAndLocationNear(SearchRequest request, Perimeter perimeter, Point point) {
        return service.findByZipCodeAndLocation(request.getZipCode(), point, perimeter.getRadiusKm())
                .stream().map(ChargingStationMapper::dto).collect(Collectors.toList());
    }

    private List<ChargingStationDto> geoSpatialSearch(Perimeter perimeter, Point point) {
        return service.findNearLocation(point, perimeter.getRadiusKm())
                .stream().map(ChargingStationMapper::dto)
                .collect(Collectors.toList());
    }


}
