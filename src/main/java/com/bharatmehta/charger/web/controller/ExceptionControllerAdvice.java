package com.bharatmehta.charger.web.controller;

import com.bharatmehta.charger.exception.ChargingStationNotFoundException;
import com.bharatmehta.charger.exception.DuplicateChargingStationException;
import com.bharatmehta.charger.web.transferobject.ChargerError;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;


/**
 * Exception handler for {@link ChargingStationController}
 */
@RestControllerAdvice
@RequiredArgsConstructor
public class ExceptionControllerAdvice {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionControllerAdvice.class);


    @ExceptionHandler(DuplicateChargingStationException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ChargerError handleDuplicateChargingStationException(DuplicateChargingStationException e) {
        return new ChargerError(e.getMessage());
    }


    @ExceptionHandler({HttpMessageConversionException.class, IllegalArgumentException.class,
            ConstraintViolationException.class, MethodArgumentNotValidException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ChargerError handleDeserializationError(Exception e) {
        if (e instanceof IllegalArgumentException ||e instanceof MethodArgumentNotValidException) {
            return new ChargerError(e.getMessage());
        }
        return ChargerError.INVALID_REQUEST_FORMAT;
    }

    @ExceptionHandler(ChargingStationNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleChargingStationNotFoundException(ChargingStationNotFoundException e) {
         LOGGER.debug(e.getMessage(),e);
    }



    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public void handleException(Throwable t) {
        LOGGER.error("Unexpected Error", t);
    }
}
