package com.bharatmehta.charger.repository;

import com.bharatmehta.charger.model.ChargingStation;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Repository for CRUD operations on ChargingStation
 */
@Repository
public interface ChargingStationRepository extends MongoRepository<ChargingStation, String> {

    /**Finds all ChargingStation near a Geolocation within a radiusKm
     * @param point Geolocation
     * @param distance Radius
     * @return List
     */
    List<ChargingStation> findByLocationNear(Point point, Distance distance);

    /**
     * Finds a ChargingStation by its uniqueIdentifier
     * @param uniqueIdentifier
     * @return Optional
     */
    Optional<ChargingStation> findByUniqueIdentifier(String uniqueIdentifier);

    /**
     * Finds all ChargingStation in a zipCode
     * @param zipCode ZipCode
     * @return List
     */
    List<ChargingStation> findAllByZipCode(String zipCode);

    /**
     * Finds all ChargingStation in a zipCode and within a  radiusKm from a Geolocation
     * @param zipCode  ZipCode
     * @param point Geolocation
     * @param distance Radius
     * @return List
     */
    List<ChargingStation> findByZipCodeAndLocationNear(String zipCode, Point point, Distance distance);
}
