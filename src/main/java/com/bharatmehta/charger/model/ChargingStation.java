package com.bharatmehta.charger.model;

import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Charging Station.A ChargingStation must have non empty uniqueIdentifier,zipCode.
 * ZipCode is not restrictive to Alphabets, for better international support.
 */
@Getter
@Setter
@Document(collection = "chargingstations")
public class ChargingStation {

    /**
     * Maximum Latitude
     */
    public static final int MAX_LATITUDE = 90;

    /**
     * Minimum Latitude
     */
    public static final int MIN_LATITUDE = -90;

    /**
     * Maximum Longitude
     */
    public static final int MAX_LONGITUDE = 180;

    /**
     * Minimum Longitude
     */
    public static final int MIN_LONGITUDE = -180;

    @Id
    public ObjectId _id;

    @Indexed(name = "chargingstations.uniqueIdentifier.index", unique = true)
    private String uniqueIdentifier;

    @NotNull
    @GeoSpatialIndexed(name = "chargingstations.location.index",
            type = GeoSpatialIndexType.GEO_2DSPHERE)
    private GeoJsonPoint location;


    @NotBlank(message = "zipCode can't be null or empty")
    private String zipCode;


    /**
     * Returns true if a ChargingStation is valid
     * @return Boolean
     */
    public boolean isLocationValid(){
        boolean result = true;
        if(location == null){
            result = false;
        }
        if(!(location.getX() >= MIN_LONGITUDE &&
        location.getX() <= MAX_LONGITUDE &&
        location.getY() >= MIN_LATITUDE &&
        location.getY() <= MAX_LATITUDE)){
            result = false;
        }
        return result;
    }

}
