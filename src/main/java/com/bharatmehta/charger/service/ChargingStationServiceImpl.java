package com.bharatmehta.charger.service;

import com.bharatmehta.charger.model.ChargingStation;
import com.google.common.base.Preconditions;
import com.bharatmehta.charger.exception.ChargingStationNotFoundException;
import com.bharatmehta.charger.exception.DuplicateChargingStationException;
import com.bharatmehta.charger.repository.ChargingStationRepository;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;

/**
 * Implementation of {@link ChargingStationService}
 */
@Service
public class ChargingStationServiceImpl implements ChargingStationService {


    private final ChargingStationRepository chargingStationRepository;


    public ChargingStationServiceImpl(final ChargingStationRepository repository) {
        Preconditions.checkArgument(repository != null, "repository can't be null");
        this.chargingStationRepository = repository;
    }


    @Override
    public ChargingStation create(final ChargingStation station) {
        validate(station);
        Optional<ChargingStation> byId = findByUniqueIdentifier(station.getUniqueIdentifier());
        if (byId.isPresent()) {
            throw new DuplicateChargingStationException(station.getUniqueIdentifier());
        }
        return chargingStationRepository.save(station);
    }

    @Override
    public void remove(final String uniqueIdentifier) {
        Optional<ChargingStation> byId = findByUniqueIdentifier(uniqueIdentifier);
        if (!byId.isPresent()) {
            throw new ChargingStationNotFoundException();
        } else {
            chargingStationRepository.deleteById(byId.get().get_id().toString());
        }
    }


    @Override
    public Optional<ChargingStation> findByUniqueIdentifier(final String identifier) {
        Preconditions.checkArgument(StringUtils.hasText(identifier), "identifier can't be null or empty");
        return chargingStationRepository.findByUniqueIdentifier(identifier);
    }


    @Override
    public List<ChargingStation> findNearLocation(final Point point, final double radiusInKm) {

        validatePerimeter(point, radiusInKm);
        return chargingStationRepository.findByLocationNear(point, new Distance(radiusInKm, Metrics.KILOMETERS));

    }

    @Override
    public List<ChargingStation> findByZipCode(final String zipCode) {
        Preconditions.checkArgument(StringUtils.hasText(zipCode), "zipCode can't null or empty");
        return chargingStationRepository.findAllByZipCode(zipCode);
    }

    @Override
    public List<ChargingStation> findByZipCodeAndLocation(final String zipCode,
                                                          final Point point, final double radiusInKm) {
        Preconditions.checkArgument(StringUtils.hasText(zipCode), "zipCode can't null or empty");
        validatePerimeter(point, radiusInKm);
        return chargingStationRepository.findByZipCodeAndLocationNear(zipCode, point,
                new Distance(radiusInKm, Metrics.KILOMETERS));

    }


    @Override
    public ChargingStation update(final ChargingStation station) {
        validate(station);
        Optional<ChargingStation> byId = findByUniqueIdentifier(station.getUniqueIdentifier());
        if (byId.isPresent()) {
            ChargingStation old = byId.get();
            old.setLocation(station.getLocation());
            old.setZipCode(station.getZipCode());
            return chargingStationRepository.save(old);
        }
        throw new ChargingStationNotFoundException();

    }


    private void validate(ChargingStation station) {
        Preconditions.checkArgument(station != null, "station can't be null");
        Preconditions.checkArgument(StringUtils.hasText(station.getUniqueIdentifier()), "uniqueIdentifier can't be null or empty");
        Preconditions.checkArgument(station.getLocation() != null, "coordinates can't be null");
        Preconditions.checkArgument(station.isLocationValid(), "Invalid coordinates");
        Preconditions.checkArgument(StringUtils.hasText(station.getZipCode()), "zipCode can't be null or empty");
    }

    private void validatePerimeter(Point point, double radiusInKm) {
        Preconditions.checkArgument(point.getX() >= ChargingStation.MIN_LONGITUDE && point.getX() <= ChargingStation.MAX_LONGITUDE);
        Preconditions.checkArgument(point.getY() >= ChargingStation.MIN_LATITUDE && point.getY() <= ChargingStation.MAX_LATITUDE);
        Preconditions.checkArgument(radiusInKm >= 0, "Radius can't be negative");
    }

}
