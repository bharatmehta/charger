package com.bharatmehta.charger.service;

import com.bharatmehta.charger.model.ChargingStation;
import com.bharatmehta.charger.exception.ChargingStationNotFoundException;
import com.bharatmehta.charger.exception.DuplicateChargingStationException;
import org.springframework.data.geo.Point;

import java.util.List;
import java.util.Optional;


/**
 * Service/Business logic for CRUD operations on {@link ChargingStation}
 */
public interface ChargingStationService {

    /**
     * Creates a new ChargingStation
     * @param station {@link ChargingStation}
     * @return ChargingStation
     * @throws IllegalArgumentException If ChargingStation is null
     * @throws IllegalArgumentException If ChargingStation has empty or null zipCode
     * @throws IllegalArgumentException If ChargingStation has empty or null unidentifier
     * @throws IllegalArgumentException If ChargingStation has invalid geo coordinates
     * @throws DuplicateChargingStationException If ChargingStation already exists with same uniqueIdentifier
     */
    ChargingStation create(ChargingStation station);

    /**
     * Removes a ChargingStation. Currently deletions are HARD
     * @param uniqueIdentifier Unique Identifier of the ChargingStation
     * @throws IllegalArgumentException If uniqueIdentifier is null or blank
     */
    void remove(String uniqueIdentifier);

    /**
     * Finds a ChargingStation by its uniqueIdentier
     * @param identifier UniqueIdentifier of the ChargingStation
     * @throws IllegalArgumentException If uniqueIdentifier is null or blank
     * @return Optional
     */
    Optional<ChargingStation> findByUniqueIdentifier(String identifier);

    /**
     * Finds ChargingStation within a perimeter defined by radiusKm in Kilometers
     * @param point Geolocation
     * @param radiusInKm Radius in Kms
     * @throws  IllegalArgumentException If point is null or invalid.
     * @throws  IllegalArgumentException If radiusKm is negative
     * @return List
     */
    List<ChargingStation> findNearLocation(Point point, double radiusInKm);

    /**
     * Finds ChargingStation with a matching zipCode
     * @param zipCode String
     * @throws IllegalArgumentException If zipCode is empty or null
     * @return List
     */
    List<ChargingStation> findByZipCode(String zipCode);

    /**
     * Finds ChargingStation within a perimeter defined by radiusKm in Kilometers in a zipCode
     * @param zipCode ZipCode
     * @param point Geolocation
     * @param radiusInKm Radius in Kms
     * @throws IllegalArgumentException If zipCode is empty or null
     * @throws  IllegalArgumentException If point is null or invalid.
     * @throws  IllegalArgumentException If radiusKm is negative
     * @return List
     */
    List<ChargingStation> findByZipCodeAndLocation(String zipCode,
                                                   Point point, double radiusInKm);


    /**
     * Updates an existing ChargingStation
     * @param station ChargingStation
     * @throws ChargingStationNotFoundException If ChargingStation doesn't exist
     * @throws IllegalArgumentException If ChargingStation is null
     * @throws IllegalArgumentException If ChargingStation has empty or null zipCode
     * @throws IllegalArgumentException If ChargingStation has empty or null unidentifier
     * @throws IllegalArgumentException If ChargingStation has invalid geo coordinates
     * @return ChargingStation
     */
    ChargingStation update(ChargingStation station);
}
