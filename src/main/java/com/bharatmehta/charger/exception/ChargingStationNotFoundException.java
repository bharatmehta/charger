package com.bharatmehta.charger.exception;

import com.bharatmehta.charger.model.ChargingStation;

/**
 * Exception when a {@link ChargingStation} is not found
 */
public class ChargingStationNotFoundException extends RuntimeException {

    private static final String MESSAGE = "ChargingStation not found";

    public ChargingStationNotFoundException(){
        super(MESSAGE);
    }

}
