package com.bharatmehta.charger.exception;


import com.bharatmehta.charger.model.ChargingStation;

/**
 * Exception when create request for {@link ChargingStation}
 * has a duplication uniqueIdentifier
 */
public class DuplicateChargingStationException extends RuntimeException {

    private static final long serialVersionUID = -7490430327139722977L;

    private static final String MESSAGE = "ChargingStation with Id:%s already exist";


    public DuplicateChargingStationException(final String duplicateId) {
        super(String.format(MESSAGE, duplicateId));
    }


}
